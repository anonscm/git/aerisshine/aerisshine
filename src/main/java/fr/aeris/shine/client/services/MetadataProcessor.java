package fr.aeris.shine.client.services;

import java.io.File;
import java.util.Date;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.file.GenericFileMessage;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;

@Component
public class MetadataProcessor implements Processor {

	@Value("${service.login}")
	private String login;

	@Value("${service.password}")
	private String password;

	@Value("${service.url.addOrUpdateWithId}")
	private String addOrUpdateWithIdUrl;

	@Override
	public void process(Exchange exchange) throws Exception {
		GenericFileMessage<File> aux = (GenericFileMessage<File>) exchange
				.getIn();
		String filePath = aux.getGenericFile().getAbsoluteFilePath();
		File srcFile = new File(filePath);
		String content = FileUtils.readFileToString(srcFile);

		MultivaluedMap formData = new MultivaluedMapImpl();
		formData.add("src", content);
		formData.add("format", "RBV");
		formData.add("login", login);
		formData.add("password", password);
		//
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(addOrUpdateWithIdUrl);
		ClientResponse response = service.type(
				MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(
				ClientResponse.class, formData);
		Status responseStatus = response.getClientResponseStatus();

		if (responseStatus.getStatusCode() != Response.Status.OK
				.getStatusCode()) {
			MetadataProcessException exception = new MetadataProcessException();
			exception.setFileName(srcFile.getName());
			exception.setFileContent(content);
			exception.setErrorTime(new Date().toString());
			exception.setStatusCode("" + responseStatus.getStatusCode());
			throw exception;
		}
	}
}
