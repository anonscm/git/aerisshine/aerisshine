package fr.aeris.shine.client.services;

import static org.slf4j.LoggerFactory.getLogger;

import java.io.File;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

@Component
public class MetadataProcess extends RouteBuilder {

	@Value("${files.in}")
	private String inputDirName;

	@Value("${files.done}")
	private String outputDirName;

	@Value("${files.ko}")
	private String koDirName;

	@Value("${service.url.version}")
	private String versionUrl;

	@Autowired
	private MetadataProcessor processor;

	private static final Logger LOG = getLogger(MetadataProcess.class);

	@Override
	public void configure() throws Exception {
		LOG.debug("MetadataProcess initialization");
		File inputDir = new File(inputDirName);
		if (inputDir.exists() == false) {
			inputDir.mkdirs();
		}

		File outputDir = new File(outputDirName);
		if (outputDir.exists() == false) {
			outputDir.mkdirs();
		}

		File koDir = new File(koDirName);
		if (koDir.exists() == false) {
			koDir.mkdirs();
		}

		if (testConnectionAvailable()) {

		}
		// errorHandler(noErrorHandler());
		onException(MetadataProcessException.class).to(
				"direct:exceptionOccured");

		from(
				"file:///{{files.in}}?antInclude={{files.pattern}}&move={{files.done}}&moveFailed={{files.ko}}")
				.log("Processing file ${file:onlyname}").process(processor)
				.log("${file:onlyname} : success ")
				.onException(MetadataProcessException.class)
				.to("direct:exceptionOccured");

		from("direct:exceptionOccured").log("An error has occured");

	}

	private boolean testConnectionAvailable() {
		LOG.debug("Starting testing url: " + versionUrl);
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(versionUrl);
		ClientResponse response = service.accept(MediaType.TEXT_HTML).get(
				ClientResponse.class);
		boolean result = (response.getClientResponseStatus().getStatusCode() == Response.Status.OK
				.getStatusCode());
		LOG.debug("Result testing url: "
				+ response.getClientResponseStatus().getStatusCode());
		return result;

	}
}